var express = require("express");
var bodyParser = require("body-parser");
var mysql = require("mysql");
var cookieParser = require("cookie-parser");
var crypto = require("crypto");
var session = require("express-session");
var sys = require("util");
var fs = require("fs");
var exec = require("child_process").exec;

var app = express();


app.use(express.static(__dirname + "/static"));
app.set('views', __dirname + '/static/views');

app.use(session ({
	secret: "secret",
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.engine('html', require('ejs').renderFile);

var connection = mysql.createConnection ({
	host: "eu-cdbr-west-01.cleardb.com",
	user: "bea6f9a654dfba",
	password: "bb0bf596",
	database: "heroku_1b9b2830204e00f"
});

setInterval(function () {
    connection.query("SELECT 1");
}, 5000);

////////////* Database START *////////////
/*
var db_config = {
	host: "eu-cdbr-west-01.cleardb.com",
	user: "bea6f9a654dfba",
	password: "bb0bf596",
	database: "heroku_1b9b2830204e00f"
};

var connection;

function handleDisconnect() {
	connection = mysql.createConnection(db_config);

	connection.connect(function(error) {				// The server is either down
		if(error) {										// or restarting (takes a while sometimes).
			console.log('error when connecting to db:', error);
			setTimeout(handleDisconnect, 2000);			// We introduce a delay before attempting to reconnect,
		}												// to avoid a hot loop, and to allow our node script to
	});													// process asynchronous requests in the meantime.
														// If you're also serving http, display a 503 error.
	connection.on('error', function(error) {
		console.log('db error', error);
		if(error.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
			handleDisconnect();							// lost due to either server restart, or a
		} else {										// connnection idle timeout (the wait_timeout
			throw error;								// server variable configures this)
		}
	});
}

handleDisconnect();
*/
////////////* Database END *///////////////

app.listen(process.env.PORT || 3000, function () {
	console.log("Listening on port 3000");
});

app.get("/error", function (request, response) {
	response.sendFile(__dirname + "/static/views/error.html");
});

app.post("/returnInfo", function (request, response) {
	if (request.body.code == 123) response.send({data: request.session});
	else response.status(401).json({error: "Authorization at returnInfo"});
});

app.get("/logout", function (request, response) {
	var username = request.session.username;
	request.session.destroy();
	console.log("User: '" + username + "' successfully logged out!");
	response.redirect("/");
});

app.post("/authHome", function (request, response) {
	var data = request.body.data;

	if (data == "home"){
		var sess = request.session;
		var responseData = {username: sess.username, sessid: sess.sessid};
		response.send(responseData);
	}
});

app.post("/fileOwners", function (request, response) {
	var unique_id = request.body["unique_id"];

	returnOwners(unique_id, function (error, data) {
		response.setHeader('Content-Type', 'application/json');
		response.send(JSON.stringify(data));
	});
});

app.post("/compileFile", function (request, response) {
	var unique_id = request.body["unique_id"];

	returnFile(unique_id, function (error, data) {
		var code = decodeURIComponent(data[0]["value"]);

		fs.writeFile(__dirname + "/tmp/tmp.cpp", code, function(err) {
			if(err) {
				return console.log(err);
			}

			var result = [], i = 0;
			function puts(error, stdout, stderr) {
				result[i] = stdout;
				i++;
			}

			var path = __dirname + "/tmp/tmp.cpp"
			//exec("g++ " + path + " -o tmp12345", puts);
			exec("gcc -Wall " + path + " -o tmp.out", puts);
			setTimeout(function() {
				exec("./tmp.out", puts);
			}, 250);
			setTimeout(function() {
				response.send(result[1]);
			}, 500);
		}); 
	});
});

app.get("/404", function (request, response) {
	response.status(404).sendFile(__dirname + "/static/views/404.html");
});

////////////* Login routes START *////////////

app.post("/login", function (request, response) {
	var username = request.body.username;
	var password = request.body.password;
	var sess = request.session;

	if (username != undefined && password != undefined) {

		/* if (userExists(username)){
			var query = connection.query("SELECT * FROM users WHERE username='" + username + "';", function(error, result) {
				//console.log(result);
			});
		} else {
			console.log("User doesn't exists!");
		} */
		userExists(username, function (error, data) {
			var returns = 0;
			if (error) {
				console.log("USER EXISTS ERROR : ",err);   
			} else {
				db_username = "prazno";
				db_password = "prazno";
				db_sessid = "prazno";
				if (data == username) {
					
					var query = connection.query("SELECT * FROM users WHERE username='" + username + "';", function (error, result) {
						var str = JSON.stringify(result);
						result = JSON.parse(str);
						
						var db_username = result[0].username;
						var db_password = result[0].password;
						var db_sessid = result[0].sessid;

						password = crypto.createHash("md5").update(password).digest("hex");

						if (db_username == username && db_password == password){
							console.log("User: '" + username + "' successfully logged in!");
							request.session.sessid = db_sessid;
							request.session.username = db_username;
							var referer = request.headers.referer;
							referer = referer.substring(0, referer.length - 6);
							referer = referer.substring(referer.length - 1);
							if (referer == "m") response.send("/m/home");
							else response.send("/home");
						}else{
							console.log("User: '" + username + "' entered wrong password!");
							response.send("/error?error=924");
						}
						//response.render("login.html");
					});

				} else {
					console.log("Entered user: '" + username + "' does not exist!");
					response.send("error?error=956");
				}
			}
		});

	} else {
		console.log("Username: '" + username + "', password: '" + password + "', one of them is undefined");
		response.send("error?error=925");
	}
});

////////////* Login routes END *//////////////


////////////* Register routes START *////////////

app.post("/register", function (request, response) {
	var username = request.body.username;
	var password = request.body.password;

	if (username != undefined && password != undefined) {

		// FOR some reason, this is unnecessary

		var sessid = randomValueHex(32);
		password = crypto.createHash("md5").update(password).digest("hex");
		console.log("Password", password);
		console.log(sessid);
		var emptyObject = "[]";
		var post = {id: null, username: username, password: password, sessid: sessid, shared: '[]'};
		var query = connection.query("INSERT INTO users SET ?", post, function (error, result) {
			if (error) {
				if (error.code == "ER_DUP_ENTRY") {
					console.log(error.code, "for user:", username);

					response.send("/error?error=409");
					response.end();
				}else{
					console.log(error);
				}
			} else {
				console.log("User successfully added to database!");
				response.send({ error: 200, username: username });
				response.end();
			}
		});
	} else {
		//response.sendFile(__dirname + "/static/views/error.html?error=925");
		response.send("/error?error=925");
	}
});

////////////* Register routes END *//////////////


////////////* Secure routes START *////////////

var router = express.Router();

router.use(function (request, response, next) {
	var sess = request.session;
	var username = sess.username;
	var sessid = sess.sessid;

	var data_crypted = request.body.crypted;
	var hash = crypto.createHash("md5").update(username).digest("hex");
	if (hash == data_crypted) {
		next();
	} else {
		response.status(401).json({error: "Authorization", hash: hash, data_crypted: data_crypted});
	}
});

router.post("/saveFile", function (request, response) {
	var sessid = request.body.sessid;
	var fileNames = request.body.fileName;
	var value = request.body.value;

	var username = request.session.username;

	if (fileNames.indexOf(".") > -1) {
		var fileName = fileNames.substring(0, fileNames.indexOf("."));
		var fileExtension = fileNames.substr(fileNames.indexOf(".") + 1);
	}else {
		var fileName = fileNames;
		var fileExtension = "txt";
	}

	var long_hash = crypto.createHash('md5').update(fileName + fileExtension + username).digest('hex');

	longHashExists(username, function (error, data) {
		console.log("error:", error, "data:", data);
		var exists = 0;
		for (var i = 0; i < data.length; i++){
			if (long_hash == data[i]["unique_id"]){
				exists = 1;
			}
		}

		if (exists == 0){
			var query = connection.query("INSERT INTO files (id, value, name, extension, owner, salt, unique_id, owners) VALUES (null, '" + value + "', '" + fileName + "', '" + fileExtension + "', '" + username + "', '" + sessid + "', '" + long_hash + "', '{ \"owner\": \"" + username + "\", \"others\":[] }')", function(error, result) {
				if (error){
					console.log("Insert error:", error);
				}
				console.log("Insert: " + result);
				response.status(200).send(long_hash);
			});
		}else{
			var query = connection.query("UPDATE files SET value='" + value + "' WHERE unique_id='" + long_hash + "';", function(error, result) {
				if (error){
					console.log("Update error:", error);
				}
				console.log("Update: " + result);
				response.status(201).send(long_hash);
			});
		}
	});


	// INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE name="A", age=19

});

router.post("/saveFile2", function (request, response) {
	var fileNames = request.body.fileName;
	var value = request.body.value;
	var sessid = request.body.sessid;
	var username = request.session.username;

	if (fileNames.indexOf(".") > -1) {
		var fileName = fileNames.substring(0, fileNames.indexOf("."));
		var fileExtension = fileNames.substr(fileNames.indexOf(".") + 1);
	} else {
		var fileName = fileNames;
		var fileExtension = "txt";
	}

	var long_hash = randomValueHex(32);

	var query = connection.query("INSERT INTO files (id, value, name, extension, owner, salt, unique_id, owners) VALUES (null, '" + value + "', '" + fileName + "', '" + fileExtension + "', '" + username + "', '" + sessid + "', '" + long_hash + "', '{ \"owner\": \"" + username + "\", \"others\":[] }')", function(error, result) {
		if (error){
			console.log("Insert error:", error);
		}
		console.log("Insert: " + result);
		response.status(200).send(long_hash);
	});

	
});

router.post("/updateFile", function (request, response) {
	var fileNames = request.body.fileName;
	var value = request.body.value;
	var unique_id = request.body.unique_id;
	var username = request.session.username;

	if (fileNames.indexOf(".") > -1) {
		var fileName = fileNames.substring(0, fileNames.indexOf("."));
		var fileExtension = fileNames.substr(fileNames.indexOf(".") + 1);
	} else {
		var fileName = fileNames;
		var fileExtension = "txt";
	}

	var query = connection.query("UPDATE files SET value='" + value + "', name='" + fileName + "', extension='" + fileExtension + "' WHERE unique_id='" + unique_id + "';", function(error, result) {
		if (error){
			console.log("Update error:", error);
		}
		console.log("'" + unique_id + "' updated by:", username);
		response.status(201).send(unique_id);
	});

	
});

router.post("/shareFile", function (request, response) {
	var unique_id = request.body["unique_id"];
	var user = request.body["user"];
	var username = request.session.username;

	userExists(user, function (error, data) {
		if (data != null && data != username) {
			returnOwners(unique_id, function (error, data) {
				var currentOwners = data.owners;
				currentOwners = JSON.parse(currentOwners);
				currentOwners.others.push({user: user});
				var newOwners = JSON.stringify(currentOwners);
				var query = connection.query("UPDATE files SET owners='" + newOwners + "' WHERE unique_id='" + unique_id + "';", function(error, result) {
					if (error){
						console.log("Update owners error:", error);
					}
					returnShared(user, function (error, data) {
						var currentShared = data.shared;
						currentShared = JSON.parse(currentShared);
						if (!currentShared.includes(unique_id)) {
							currentShared.push(unique_id);
							var newShared = JSON.stringify(currentShared);
							
							var query2 = connection.query("UPDATE users SET shared='" + newShared + "' WHERE username='" + user + "';", function(error2, result2) {
								if (error2){
									console.log("Update shared error:", error2);
								}
								console.log("Update shared: " + result2);
							});
						}
					});
				});
			});
		}		
	});
});

router.post("/shareFile2", function (request, response) {
	var unique_id = request.body["unique_id"];
	var user = request.body["user"];
	var username = request.session.username;

	userExists(user, function (error, data) {
		if (data != null && data != username) {
			returnOwners(unique_id, function (error, data) {
				var currentOwners = data.owners;
				currentOwners = JSON.parse(currentOwners);
				currentOwners.others.push({user: user});
				var newOwners = JSON.stringify(currentOwners);
				var query = connection.query("UPDATE files SET owners='" + newOwners + "' WHERE unique_id='" + unique_id + "';", function(error, result) {
					if (error){
						console.log("Update owners error:", error);
					}
					returnShared(user, function (error, data) {
						var currentShared = data.shared;
						currentShared = JSON.parse(currentShared);
						if (!(currentShared.indexOf(unique_id) > -1)) {
							currentShared.push(unique_id);
							var newShared = JSON.stringify(currentShared);
							
							var query2 = connection.query("UPDATE users SET shared='" + newShared + "' WHERE username='" + user + "';", function(error2, result2) {
								if (error2){
									console.log("Update shared error:", error2);
								}
								console.log("Update shared: " + result2);
								response.send(unique_id);
							});
						}
					});
				});
			});
		}		
	});
});

router.post("/list", function (request, response) {
	var username = request.session.username;

	returnFiles(username, function (error, data) {
		response.setHeader('Content-Type', 'application/json');
		response.send(JSON.stringify(data));
	});

});

router.post("/shared", function (request, response) {
	var username = request.session.username;

	returnFiles2(username, function (error, data) {
		response.setHeader('Content-Type', 'application/json');
		response.send(JSON.stringify(data));
	});

});

router.post("/file", function (request, response) {
	//var unique_id = request.session.username;
	var unique_id = request.body["unique_id"];

	returnFile(unique_id, function (error, data) {
		response.setHeader('Content-Type', 'application/json');
		response.send(JSON.stringify(data));
	});

});

router.post("/fileInfo", function (request, response) {
	//var unique_id = request.session.username;
	var unique_id = request.body["unique_id"];

	returnFile(unique_id, function (error, data) {
		response.setHeader('Content-Type', 'application/json');
		response.send(JSON.stringify(data));
	});

});

router.post("/deleteFile", function (request, response) {
	var unique_id = request.body["unique_id"];
	var query = connection.query("DELETE FROM files WHERE unique_id='" + unique_id + "';", function (error, result) {
		if (error) console.log("/deleteFile ERROR", error);
		else {
			response.send("It works!");
		}
	});
});

app.use("/api", router);

////////////* Secure routes END *//////////////

app.get("/", function (request, response) {
	if (request.session.sessid){
		response.redirect("/home");
	}else{
		response.redirect("/login");
	}
});

app.get("/login", function (request, response) {
	if (request.session.sessid){
		response.redirect("/home");
	}else{
		response.sendFile(__dirname + "/static/views/login.html");
	}
});

app.get("/register", function (request, response) {
	if (request.session.sessid){
		response.redirect("/home");
	}else{
		response.sendFile(__dirname + "/static/views/register.html");
	}
});

app.get("/home", function (request, response) {
	if (request.session.sessid){
		response.sendFile(__dirname + "/static/views/home.html");
	}else{
		response.redirect("/login");
	}
});

app.get("/list", function (request, response) {
	if (request.session.sessid){
		response.sendFile(__dirname + "/static/views/list.html");
	}else{
		response.redirect("/login");
	}
});

app.get("/shared", function (request, response) {
	if (request.session.sessid){
		response.sendFile(__dirname + "/static/views/shared.html");
	}else{
		response.redirect("/login");
	}
});

app.get("/new", function (request, response) {
	if (request.session.sessid){
		response.sendFile(__dirname + "/static/views/new.html");
	}else{
		response.redirect("/login");
	}
});

app.get("/compile", function (request, response) {
	if (request.session.sessid){
		response.sendFile(__dirname + "/static/views/compile.html");
	}else{
		response.redirect("/login");
	}
});

app.get("*", function (request, response) {
	response.status(404).sendFile(__dirname + "/static/views/404.html");
});

/**************** Functions ******************/

function connectToDatabase () {
	connection.connect (function(err) {
		if (err){
			console.log("Error connecting to database", err);
		} else {
			console.log("Successfully connected to database");
		}
	});
}

function userExists (username, callback) {
	var query = connection.query("SELECT username FROM users WHERE username='" + username + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result[0]) {
			if (result[0].username == username) {
				callback(null,result[0].username);
			} else {
				callback(null,null);
			}
		} else {
			callback(null,null);
		}
	});
}

function sessidExists (sessid, callback) {
	var query = connection.query("SELECT sessid FROM users WHERE sessid='" + sessid + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result[0]) {
			if (result[0].sessid == sessid) {
				callback(null,result[0].sessid);
			} else {
				callback(null,null);
			}
		} else {
			callback(null,null);
		}
	});
}

function longHashExists (username, callback) {
	var query = connection.query("SELECT unique_id FROM files WHERE owner='" + username + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result) {
			callback(null, result);
		} else {
			callback(null,null);
		}
	});
}

function returnFiles (username, callback) {
	var query = connection.query("SELECT * FROM files WHERE owner='" + username + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result) {
			callback(null, result);
		} else {
			callback(null,null);
		}
	});
}

function returnFiles2 (username, callback) {
	var query = connection.query("SELECT shared FROM users WHERE username='" + username + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result) {
			callback(null, result);
		} else {
			callback(null,null);
		}
	});
}

function returnFile (unique_id, callback) {
	var query = connection.query("SELECT * FROM files WHERE unique_id='" + unique_id + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result) {
			callback(null, result);
		} else {
			callback(null,null);
		}
	});
}

function returnOwners (unique_id, callback) {
	var query = connection.query("SELECT * FROM files WHERE unique_id='" + unique_id + "';", function (error, result) {
		if (result) {
			callback(null, result[0]);
		} else {
			callback(null,null);
		}
	});
}

function returnShared (username, callback) {
	var query = connection.query("SELECT * FROM users WHERE username='" + username + "';", function (error, result) {
		if (result) {
			callback(null, result[0]);
		} else {
			callback(null,null);
		}
	});
}

//function deleteShared

function randomValueHex (len) {
	return crypto.randomBytes(Math.ceil(len/2))
	.toString('hex') // convert to hexadecimal format
	.slice(0,len);   // return required number of characters
}

var sha512 = function(password, salt){
    var hash = crypto.createHmac("sha512", salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest("hex");
    return {
        salt:salt,
        passwordHash:value
    };
};
/**********************************************/