#include <cstdio>
#include <cstdlib>

int main (void) {
	int x = 64;
	double y;
	
	while (y * y <= x) {
		y += 0.001;
	}
	printf("%.2f", y);
	return EXIT_SUCCESS;
}
