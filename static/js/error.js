var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var error = getUrlParameter("error");

if (error == undefined || error == "") {
	alert("Error is empty!");
	window.location.href = "/home";
}

var errors = {409: "This username is already registered!", 925: "Username and/or password hasn't been entered!", 956: "User does not exist!", 924: "Wrong password!", 313: "File name cannot be empty!"};

alert(errors[error]);
window.location = document.referrer;
//alert(error);

