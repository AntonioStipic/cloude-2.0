function noPermission () {
	alert("You do not have permission to operate this file!");
}

function saveFile () {
	var fileName = document.getElementById("fileName2").value;
	var textareaValue = encodeURIComponent(document.getElementById("textarea2").value);
	var sessid = document.getElementById("sessid").innerHTML;
	var crypted = document.getElementById("crypted").innerHTML;

	if (fileName == "" || fileName == ".") {
		window.location.href = "/error?error=313";
	}

	textareaValue = textareaValue.replace("'", "\\'")
	var data = {sessid: sessid, crypted: crypted, fileName: fileName, value: textareaValue};

	$.ajax({
		type: "POST",
		url: "/api/saveFile2",
		data: data,
		dataType: "binary",
		error: function (data) {
			console.log("ERROR", data);
			window.location.href = "/home?file=" + data["responseText"];
		}
	});

}

function shareFile (crypted, unique_id) {
	var newUser = prompt("Enter username of the user you want to share file with:");
	var data = {unique_id: unique_id, crypted: crypted, user: newUser};
	$.ajax({
		type: "POST",
		url: "/api/shareFile2",
		data: data,
		dataType: "binary",
		error: function (data) {
			console.log("ERROR", data);
			window.location.href = "/home?file=" + data["responseText"];
		}
	});
}

function updateFile (unique_id, crypted) {
	document.getElementById("loading-small").style.display = "inline";
	var fileName = document.getElementById("fileName").innerHTML;

	if (fileName == "" || fileName == ".") {
		window.location.href = "/error?error=313";
	}

	var textareaValue = encodeURIComponent(document.getElementById("textarea").value);
	textareaValue = textareaValue.replace("'", "\\'")
	textareaValue = textareaValue.replace('"', '\\"')
	var data = {fileName: fileName, value: textareaValue, unique_id: unique_id, crypted: crypted};

	$.ajax({
		type: "POST",
		url: "/api/updateFile",
		data: data,
		dataType: "binary",
		error: function (data) {
			console.log("ERROR", data);
			window.location.href = "/home?file=" + data["responseText"];
		}
	});
}

function openFile (unique_id) {
	window.location.href = "/home?file=" + unique_id;
}

function deleteFile (unique_id, crypted) {
	var answer = prompt("Are you sure you want to delete this file? (Answer with Yes/No)");
	if (answer.toLowerCase() == "yes" || answer.toLowerCase() == "y") {
		var data = {unique_id: unique_id, crypted: crypted};
		$.ajax({
			type: "POST",
			url: "/api/deleteFile",
			data: data,
			success: function (data) {
				console.log("DELETE FILE", data);
				window.location.reload();
			}
		});
	}
}

var getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	sURLVariables = sPageURL.split('&'),
	sParameterName,
	i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};

$(document).delegate('#textarea', 'keydown', function(e) {
	var keyCode = e.keyCode || e.which;

	if (keyCode == 9) {
		e.preventDefault();
		var start = $(this).get(0).selectionStart;
		var end = $(this).get(0).selectionEnd;

		// set textarea value to: text before caret + tab + text after caret
		$(this).val($(this).val().substring(0, start) + "\t" + $(this).val().substring(end));

		// put caret at right position again
		$(this).get(0).selectionStart =
		$(this).get(0).selectionEnd = start + 1;
	}
});