# Cloude

Cloude is a cloud-enabled, AngularJS and Node.js powered Web application for editing any kind of files online and compiling and running C++ apps while having everything saved to cloud and prepared to be served at any time.

# Install

```
git clone https://Spaldin@bitbucket.org/Spaldin/cloude-2.0.git
cd cloude-2.0
sh script.sh
nodemon server.js

Go to url:
http://localhost:3000/
```


# Routes
```
/login -> Shows login page
/register -> Shows register page
/home -> Homepage with all the functions
/list -> List all files from current user in session
/shared -> List all files shared with the current user in the session
/new -> POST route to server that creates new file and inserts it into database
/error -> Error handling route that alerts whats the problem
/compile -> Compiles C and C++ code and returns the result
```